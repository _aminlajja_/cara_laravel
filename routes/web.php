<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
    // return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'company', 'as' => 'company', 'middleware' => 'auth'], function(){

    Route::get('/list', [App\Http\Controllers\CompaniesController::class, 'index'])->name('.list');

});

Route::group(['prefix' => 'employees', 'as' => 'employees', 'middleware' => 'auth'], function(){

    Route::get('/list', [App\Http\Controllers\EmployeesController::class, 'index'])->name('.list');

});

Route::group(['prefix' => 'announcement', 'as' => 'announcement', 'middleware' => 'auth'], function(){

    Route::get('/list', [App\Http\Controllers\AnnouncementsController::class, 'index'])->name('.list');

});
