@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                   this is company page

                    {{-- {{auth()->user()->checkAccess()}} --}}
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center pt-2">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List Of Companies</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Logo
                                    </th>
                                    <th>
                                        Website
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($list as $company)
                                    <tr>
                                        <td>
                                            {{$list->firstItem() + $loop->index}}
                                        </td>
                                        <td>
                                            {{$company->name}}
                                        </td>
                                        <td>
                                            {{$company->logo}}
                                        </td>
                                        <td>
                                            {{$company->website}}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="4">No Record</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="pagination">{{$list->links()}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
