<?php

namespace App\Http\Controllers;

use App\Models\Announcements;
use Illuminate\Http\Request;

class AnnouncementsController extends Controller
{
    public function index(){
        $list = Announcements::paginate(5);
        return view('cara.announcement.list', ['list' => $list]);
    }
}
