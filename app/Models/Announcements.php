<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
    use HasFactory;

    protected $table = 'announcements';

    protected $fillable = [
        'title',
        'description',
        'image',
        'created_*',
        'updated_*',
    ];

    protected $casts = [
        // to store json or array;
    ];
}
